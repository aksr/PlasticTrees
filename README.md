
See project homepage http://graphics.uni-konstanz.de/publikationen/2012/plastic_trees/website.

Original code from Adrien Peytavie http://liris.cnrs.fr/~apeytavi/teaching.htm.

Requires GL/glew.h, GL/glut.h, GL/gl.h and GL/glu.h.
