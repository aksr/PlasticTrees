// Changelog 14.02.2001

#ifndef __Shape__ 
#define __Shape__

#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>										
#include <GL/glu.h>

#include <iostream>

#include "vector.h"
#include "box.h"

GLuint GenerateCube();

class Node {
protected:
  Box box;
public:
  virtual void Render()=0;
  virtual void RenderVBO()=0;
  Box GetBox() { return box; } 
};

class Union:public Node {
protected:
  Node* a;
  Node* b;
public:
  Union(Node*,Node*);
  ~Union();
  virtual void Render();
  virtual void RenderVBO(){ Render(); }
};

class Cube:public Node {
protected:
  Vector a;
  Vector b;

  unsigned int	VBO_vertex;	
  unsigned int	VBO_triangle;
  int nv,nt;
public:
  Cube(const Vector&,const Vector&);
  virtual void Render();

  void CreateVBO();
  void RenderVBO();
};

class Cylinder:public Node {
protected:
  Vector a;
  Vector b;
  double r;
  int n;

  unsigned int	VBO_vertex;	
  unsigned int	VBO_triangle;
  int nv,nt;
public:
  Cylinder(const Vector&,const Vector&,const double&,int);
  virtual void Render();

  void CreateVBO();
  void RenderVBO();

  // EDIT
  Vector getA(){ return a; }
  Vector getB(){ return b; }
  double getR(){ return r; }
  int    getN(){ return n; }
};

#endif
