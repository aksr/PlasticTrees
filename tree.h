/*
**    Tree.h
**    Copyright (C) 2013 Adrien Kaiser
**    All rights reserved.
*/

#ifndef TREE_H 
#define TREE_H

#include <vector>

#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>										
#include <GL/glu.h>

#include "shape.h"
#include "opengl.h"

#ifndef PI
#define PI 3.14159265358979323846
#endif

class Rectangle : public Node
{
protected:
  Vector m_a;
  Vector m_b;

public:
  Rectangle( Vector a, Vector b );
  Vector getA(){ return m_a; }
  Vector getB(){ return m_b; }

  virtual void Render();
  virtual void RenderVBO(){ Render(); }
};

class Branch : public Cylinder
{
protected:
  std::vector<Branch*> m_sons;
  float                m_positionOnParent; // so we can replace A on parent if parent moved
  Branch*              m_parent; // so we can replace A on parent if parent moved
  bool                 m_display;
  Vector               m_restingAB; // vector A -> B : to keep track of the resting position of the branch if it is bended

public:
  Branch( Vector a, Vector b, float r, int n, Branch* parent );
  ~Branch();
  std::vector<Branch*> getSons(){ return m_sons; }
  void setDisplay(bool display){ m_display = display; }
  Branch& operator+= (const Vector&);
  void renderBranch(); // because Cylinder already has a Render() that we need to use (so no virtual)

  void generateSons( unsigned nbSteps, unsigned nbSons );
  void generateJunctionSphere();
  void adaptToEnvironment(Rectangle* wall);
};

class Tree : public Node // Node so we can put it in a union in opengl.cpp
{
protected:
  Branch*  m_trunk;

public:
  Tree( Vector a, Vector b, float r, int n, unsigned nbSons, unsigned nbSteps );
  ~Tree();
  Branch* getTrunk(){ return m_trunk; }
  Tree& operator+= (const Vector&);
  void Render();
  void RenderVBO(){ Render(); }
  void generateJunctionSpheres();
  void adaptToEnvironment( Rectangle* ground, std::vector< Rectangle* > environment );
};

#endif
