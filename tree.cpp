/*
**    Tree.cpp
**    Copyright (C) 2013 Adrien Kaiser
**    All rights reserved.
*/

#include "time.h" // for srand(time(NULL));

#include "tree.h"

// Fractal parameters
#define LENGTHRATIO             0.5 // float lengthRatio = 0.6;
#define LENGTHRATIOD            0.2 // Delta
#define RADIUSRATIO             0.6 // float radiusRatio = 0.7;
#define RADIUSRATIOD            0.2
#define POSITIONONPARENT        0.5 // float positionOnParent = 0.5;
#define POSITIONONPARENTD       0.5
#define ANGLE1                  0.5 // float angle = PI/4;
#define ANGLE1D                 0.6
#define ANGLE2                  2*PI

#define ROTATIONMAX             0.2  // translation for distance to wall == 0 
#define DISTMAXFORTRANS         1.5  // limit distance to apply rotation

#define JUNCTIONSPHERESAMPLING 10

#define WALLMAXDISTANCETRUNK    0.5   // max distance authorized from trunk to wall
#define WALLMAXDISTANCEBRANCH   0.02  // max distance authorized from any branch to wall

////////////////////// class Rectangle ////////////////////////////////

Rectangle::Rectangle( Vector a, Vector b)
{
  m_a=a;
  m_b=b;
  box=Box(a,b);
}

void Rectangle::Render()
{
  GlutShade(0.8,0.9,0.6);
  glBegin(GL_QUADS);

  // Face z=-1
  glNormal3f( 0.0f, 0.0f,-1.0f);
  glVertex3f(m_a[0],m_a[1],m_a[2]); // a
  glVertex3f(m_a[0],m_b[1],m_a[2]);
  glVertex3f(m_b[0],m_b[1],m_a[2]);
  glVertex3f(m_b[0],m_a[1],m_a[2]);

  // Face z=+1
  glNormal3f( 0.0f, 0.0f, 1.0f);
  glVertex3f(m_a[0],m_a[1],m_b[2]);
  glVertex3f(m_b[0],m_a[1],m_b[2]);
  glVertex3f(m_b[0],m_b[1],m_b[2]); // b
  glVertex3f(m_a[0],m_b[1],m_b[2]);

  // Face x=-1
  glNormal3f(-1.0f,0.0f,0.0f);
  glVertex3f(m_a[0],m_a[1],m_a[2]); // a
  glVertex3f(m_a[0],m_b[1],m_a[2]);
  glVertex3f(m_a[0],m_b[1],m_b[2]);
  glVertex3f(m_a[0],m_a[1],m_b[2]);

  // Face x=+1
  glNormal3f( 1.0f, 0.0f, 0.0f);
  glVertex3f(m_b[0],m_a[1],m_a[2]);
  glVertex3f(m_b[0],m_b[1],m_a[2]);
  glVertex3f(m_b[0],m_b[1],m_b[2]); // b
  glVertex3f(m_b[0],m_a[1],m_b[2]);

  // Face y=-1
  glNormal3f( 0.0f,-1.0f, 0.0f);
  glVertex3f(m_a[0],m_a[1],m_a[2]); // a
  glVertex3f(m_b[0],m_a[1],m_a[2]);
  glVertex3f(m_b[0],m_a[1],m_b[2]);
  glVertex3f(m_a[0],m_a[1],m_b[2]);

  // Face y=+1
  glNormal3f( 0.0f, 1.0f, 0.0f);
  glVertex3f(m_a[0],m_b[1],m_a[2]);
  glVertex3f(m_a[0],m_b[1],m_b[2]);
  glVertex3f(m_b[0],m_b[1],m_b[2]); // b
  glVertex3f(m_b[0],m_b[1],m_a[2]);

  glEnd();
}

////////////////////// class Branch ////////////////////////////////

Branch::Branch( Vector a, Vector b, float r, int n, Branch* parent ) : Cylinder( a, b, r, n )
{
  m_display          = true;
  m_parent           = parent;
  m_restingAB        = b - a;

  if( parent != NULL ) // if not the trunk
  {
    m_positionOnParent = Norm( parent->getA() - a ) / Norm( parent->getB() - parent->getA() );
  }
  else
  {
    m_positionOnParent = 0;
  }
}

Branch::~Branch()
{
  for(unsigned i=0 ; i<m_sons.size() ; i++)
  {
    delete m_sons[i]; // will delete all sons by reccurence (ends when sons.size()=0 -> end of tree)
  }

  delete this;
}

Branch& Branch::operator+= (const Vector& u)
{
  a += u;
  b += u;

  for( unsigned i=0 ; i<m_sons.size() ; i++ )
  {
    *m_sons[i] += u;
  }

  return *this;
}

void Branch::renderBranch()
{
  if( m_display )
  {
    Render(); // from class Cylinder

    for( unsigned i=0 ; i<m_sons.size() ; i++ )
    {
      m_sons[i]->renderBranch();
    }
  }
}

Vector rotateVectorAxis3D( Vector vector, Vector axis, float angle )
{
  if( Norm(axis) == 0.0 )
  {
    return vector;
  }

  axis = axis / Norm(axis);

  float x  = vector[0];
  float y  = vector[1];
  float z  = vector[2];
  float ux = axis[0];
  float uy = axis[1];
  float uz = axis[2];
  float c  = cos(angle);
  float s  = sin(angle);

  Vector newVector; // http://fr.wikipedia.org/wiki/Matrice_de_rotation
  newVector[0] = x* (ux*ux+(1-ux*ux)*c) + y* (ux*uy*(1-c)-uz*s)  + z* (ux*uz*(1-c)+uy*s);
  newVector[1] = x* (ux*uy*(1-c)+uz*s)  + y* (uy*uy+(1-uy*uy)*c) + z* (uy*uz*(1-c)-ux*s);
  newVector[2] = x* (ux*uz*(1-c)-uy*s)  + y* (uy*uz*(1-c)+ux*s)  + z* (uz*uz+(1-uz*uz)*c);

  return newVector;
}

Vector rotateVectorRandom( Vector vector )
{
  float angle1 = ANGLE1 + ANGLE1D*float(rand())/RAND_MAX; // 1st, along prependicular axis
  Vector newVector = rotateVectorAxis3D( vector, Orthogonal(vector), angle1 );

  float angle2 = ANGLE2*float(rand())/RAND_MAX; // then, along axis = vector
  newVector = rotateVectorAxis3D( newVector, vector, angle2 );

  return newVector;
}

void Branch::generateSons( unsigned nbSteps, unsigned nbSons )
{
  for(unsigned i=0 ; i<nbSons ; i++)
  {
    // random values for the fractal parameters
    float lengthRatio      = LENGTHRATIO + LENGTHRATIOD*float(rand())/RAND_MAX;
    float radiusRatio      = RADIUSRATIO + RADIUSRATIOD*float(rand())/RAND_MAX;
    float positionOnParent = POSITIONONPARENT + POSITIONONPARENTD*float(rand())/RAND_MAX;

    Vector newA = a + positionOnParent * ( b-a );
    Vector newB = newA + lengthRatio * rotateVectorRandom( b-a );
    float  newR = radiusRatio * r;
    int    newN = lengthRatio * n; // to keep same sampling of the cylinder

    Branch* newBranch = new Branch( newA, newB, newR, newN, this ); // the current branch (this) is the parent of the new branch
    m_sons.push_back( newBranch );

    if( nbSteps > 0 ) // recursivity : nbSteps-1 => build tree branch by branch and not step by step
	{
      newBranch->generateSons( nbSteps-1, nbSons );
	}
  }
}

void Branch::generateJunctionSphere() // HAS to be done in the main loop (opengl.cpp:GlutRendering())
{
  if( m_display )
  {
    glPushMatrix(); // glLoadIdentity(); // go to origin
    glTranslatef(b[0], b[1], b[2]);
    glutSolidSphere(r, JUNCTIONSPHERESAMPLING, JUNCTIONSPHERESAMPLING);
    glPopMatrix();

    for( unsigned i=0 ; i<m_sons.size() ; i++ )
    {
      m_sons[i]->generateJunctionSphere();
    }
  }
}

bool branchIntersectsWall( Vector a, Vector b, Vector aw, Vector bw )
{
  if( (a[0]+WALLMAXDISTANCEBRANCH >= aw[0] && a[0]-WALLMAXDISTANCEBRANCH <= bw[0] && a[2]+WALLMAXDISTANCEBRANCH >= aw[2] && a[2]-WALLMAXDISTANCEBRANCH <= bw[2])  // if start of branch inside the wall
   || (b[0]+WALLMAXDISTANCEBRANCH >= aw[0] && b[0]-WALLMAXDISTANCEBRANCH <= bw[0] && b[2]+WALLMAXDISTANCEBRANCH >= aw[2] && b[2]-WALLMAXDISTANCEBRANCH <= bw[2])  // if end of branch inside the wall
   || (a[0] >= bw[0] && b[0] <= aw[0] && (a[2]+WALLMAXDISTANCEBRANCH >= aw[2] && a[2]-WALLMAXDISTANCEBRANCH <= bw[2] || b[2]+WALLMAXDISTANCEBRANCH >= aw[2] && b[2]-WALLMAXDISTANCEBRANCH <= bw[2])) // if start and end of branch not on the same side of the wall
   || (a[0] <= aw[0] && b[0] >= bw[0] && (a[2]+WALLMAXDISTANCEBRANCH >= aw[2] && a[2]-WALLMAXDISTANCEBRANCH <= bw[2] || b[2]+WALLMAXDISTANCEBRANCH >= aw[2] && b[2]-WALLMAXDISTANCEBRANCH <= bw[2])) )
  {
    return true;
  }
  else
  {
    return false;
  }
}

Vector bendBranch( Vector a, Vector ab, Vector aw, Vector bw, float r ) // a is a or b
{
  /* Bend branch depending on their distance to the wall.
  Distance from each branch node to the cube,
  and translate node along direction: point on cube -> node.
  Translating inverse proportionnal to the distance */

  Vector b = a + ab;

  if( a[2]+WALLMAXDISTANCEBRANCH < aw[2] || a[2]-WALLMAXDISTANCEBRANCH > bw[2] ) // if not in front of the wall
  {
    return b;
  }

  Vector wallToB;
  if( b[0] < aw[0] ) // due to previous statements, branch can NOT intersect wall
  {
    wallToB = Vector( b[0]-aw[0], 0, 0 );
  }
  else if( b[0] > bw[0] )
  {
    wallToB = Vector( b[0]-bw[0], 0, 0 );
  }
  else // if inside wall, don't move and the branch will not be displayed
  {
    return b;
  }

  if( Norm(wallToB) > 0 && Norm(wallToB) < DISTMAXFORTRANS ) // rotate b
  {
    Vector axis; // cross product between ab and bToWall
	axis[0] = ab[1]*wallToB[2] - ab[2]*wallToB[1];
	axis[1] = ab[2]*wallToB[0] - ab[0]*wallToB[2];
	axis[2] = ab[0]*wallToB[1] - ab[1]*wallToB[0];

    // ~= ROTATIONMAX for 0 and ~= 0 for DISTMAXFORTRANS, E [0, ROTATIONMAX]
	// (1-r) � [0.85 1] => rotation max for smallest branches, bigger for small branches !! TODO: if r>=1 -> depend on Rmax = Rtrunk
    float bendingAngle = (1-r) * ROTATIONMAX * ( 1/( Norm(wallToB) + (1-1/DISTMAXFORTRANS) ) - 1/DISTMAXFORTRANS );
	Vector newAB = rotateVectorAxis3D( ab, axis, bendingAngle ); // vector, axis, angle
    return a + newAB;
  }
  else // distance >= DISTMAXFORTRANS // if too far, no rotation
  {
    return b; // b will be in the resting position
  }
}

void Branch::adaptToEnvironment( Rectangle* wall )
{
  // Bend branch
   // Replace A on parent branch (in case parent has been moved)
  a = m_parent->getA() + m_positionOnParent * ( m_parent->getB() - m_parent->getA() );
   // Bend branch by moving B (the original, resting positions B, not the one that has been moved)
  b = bendBranch( a, m_restingAB, wall->getA(), wall->getB(), r ); // a, ab, aw, bw, r -> returns the new B

  // Don't display branch if intersects wall <=> display branch = NO branch intersects wall
  m_display = ! branchIntersectsWall( a, b, wall->getA(), wall->getB() );

  // Recursively for all sons
  for( unsigned i=0 ; i<m_sons.size() ; i++ )
  {
    m_sons[i]->adaptToEnvironment( wall );

    if( !m_display ) // done after adaptToEnvironment() so not displayed if outside wall but parent not displayed
	{
	  m_sons[i]->setDisplay(false); // if parent not displayed, don't display sons
	}
  }
}

////////////////////// class Tree ////////////////////////////////

Tree::Tree( Vector a, Vector b, float r, int n, unsigned nbSons, unsigned nbSteps )
{
  m_trunk  = new Branch( a, b, r, n, NULL );

  srand(time(NULL)); // reset random using time since 01/01/1970 to have all rand() to be different => has to be called ONCE otherwise same values over again
  m_trunk->generateSons( nbSteps, nbSons );
}

Tree::~Tree()
{
  delete m_trunk; // delete all son branches as well, by recurrence
  delete this;
}

Tree& Tree::operator+= (const Vector& u)
{
  *m_trunk += u; // will move all other sons by recurrence
  return *this;
}

void Tree::Render()
{
  m_trunk->renderBranch();
}

void Tree::generateJunctionSpheres()
{
  m_trunk->generateJunctionSphere();
}

Vector stayInsideGround( Vector a, float  r, Vector ag, Vector bg )
{
  if( a[0]-r < ag[0] )
  {
    return Vector( ag[0]-(a[0]-r), 0, 0 );
  }
  else if( a[0]+r > bg[0] )
  {
    return Vector( bg[0]-(a[0]+r), 0, 0 );
  }

  if( a[2]-r < ag[2] )
  {
    return Vector( 0, 0, ag[2]-(a[2]-r) );
  }
  else if( a[2]+r > bg[2] )
  {
    return Vector( 0, 0, bg[2]-(a[2]+r) );
  }

  return Vector(0,0,0);
}

Vector avoidWallCollision( Vector a, float  r, Vector aw, Vector bw )
{
  if( (a[0]+r+WALLMAXDISTANCETRUNK) >= aw[0]    // if trunk too close to the wall (in the wall's neighbourhood)
   && (a[0]+r-WALLMAXDISTANCETRUNK) <= bw[0]
   && (a[2]+r+WALLMAXDISTANCETRUNK) >= aw[2]
   && (a[2]+r-WALLMAXDISTANCETRUNK) <= bw[2] )
  {
    // prevent trunk from moving -> apply reverse translation
    if( (a[0]+r+WALLMAXDISTANCETRUNK)-aw[0] < bw[0]-(a[0]+r-WALLMAXDISTANCETRUNK) ) // closer to a
	{
	  return Vector( aw[0]-(a[0]+r+WALLMAXDISTANCETRUNK), 0, 0 );
	}
	else // closer to b
	{
      return Vector( bw[0]-(a[0]+r-WALLMAXDISTANCETRUNK), 0, 0 );
	}
  }

  return Vector(0,0,0);
}

void Tree::adaptToEnvironment( Rectangle* ground, std::vector< Rectangle* > environment )
{
  /* Adapt tree to environment:
     - Tree stays inside the ground (trunk)
	 - Tree stays at a certain distance from the walls (trunk)
     - Branches that are close to the walls are bended to go away from it (all branches separately)
	 - Branches that intersect the walls are not displayed (all branches separately)
  */

  // Prevent tree from going out of the ground
  *this += stayInsideGround(  m_trunk->getA(), m_trunk->getR(), ground->getA(), ground->getB()  );

  for(unsigned i=0 ; i<environment.size() ; i++) // for all walls in environment
  {
    // Prevent the tree from going to close to the wall
    *this += avoidWallCollision( m_trunk->getA(), m_trunk->getR(), environment[i]->getA(), environment[i]->getB() );

    // Trunk does not move -> start with first branches (== sons of trunk)
    for(unsigned s=0 ; s<m_trunk->getSons().size() ; s++ )
    {
      m_trunk->getSons()[s]->adaptToEnvironment( environment[i] );
    }
  }
}