
#include <GL/glew.h>
#include <GL/glut.h>									
#include <GL/glu.h>
#include <GL/gl.h>	

#include <iostream>

#include "shape.h"

#include "vector.h"
#include "opengl.h"

const double Pi=3.14159265358979323846;

Cylinder::Cylinder(const Vector& a,const Vector& b,const double& r,int n)
{
  Cylinder::a=a;
  Cylinder::b=b;
  Cylinder::r=r;
  Cylinder::n=n;

  box=Box(a,b);
  //CreateVBO();
}
                
void Cylinder::Render()
{
  GlutShade(0.8,0.9,0.6);

  Vector u = b-a ;
  Vector axis=u*(1.0/Norm(u));

  // MayaFrame
  Vector x=Orthogonal(axis);
  Vector y=axis/x;

  x = x*(1.0/Norm(x));
  y = y*(1.0/Norm(y));

  glBegin(GL_QUADS);

  // Vertices and normals
  for (int i=0;i<n;i++) 
  {
    double t1=2.0*Pi*double(i)/double(n);
    Vector u1=x*cos(t1)+y*sin(t1);

    glNormal3f( u1[0], u1[1],u1[2]);
    glVertex3f( (a+r*u1)[0],(a+r*u1)[1],(a+r*u1)[2]);
    glVertex3f( (b+r*u1)[0],(b+r*u1)[1],(b+r*u1)[2]);
    
    double t2=2.0*Pi*double(i+1)/double(n);
    Vector u2=x*cos(t2)+y*sin(t2);

    glNormal3f( u2[0], u2[1],u2[2]);
    glVertex3f( (b+r*u2)[0],(b+r*u2)[1],(b+r*u2)[2]);
    glVertex3f( (a+r*u2)[0],(a+r*u2)[1],(a+r*u2)[2]);
  }

  glEnd();

  glBegin(GL_TRIANGLES);

  // Vertices and normals
  for (int i=0;i<n;i++) 
  {
    double t1=2.0*Pi*double(i)/double(n);
    Vector u1=x*cos(t1)+y*sin(t1);

    glNormal3f( u1[0], u1[1],u1[2]);
    glVertex3f( (a+r*u1)[0],(a+r*u1)[1],(a+r*u1)[2]);
    glVertex3f( (b+r*u1)[0],(b+r*u1)[1],(b+r*u1)[2]);
    
    double t2=2.0*Pi*double(i+1)/double(n);
    Vector u2=x*cos(t2)+y*sin(t2);

    glNormal3f( u2[0], u2[1],u2[2]);
    glVertex3f( (b+r*u2)[0],(b+r*u2)[1],(b+r*u2)[2]);
    glVertex3f( (a+r*u2)[0],(a+r*u2)[1],(a+r*u2)[2]);
  }

  glEnd();
}

void Cylinder::CreateVBO()
{
	nv=8;
  
  GLfloat *PointArrayObject=new GLfloat[nv*9]; // 8 Vertex *( 3 Normal + 3 Color + 3 Position)
	// Vertex 1
	// Color
	PointArrayObject[0]=0.9f;
	PointArrayObject[1]=0.1f;
	PointArrayObject[2]=0.1f;

	// Normal
	PointArrayObject[3]=-1.0f;
	PointArrayObject[4]=-1.0f;
	PointArrayObject[5]=-1.0f;

	// Position
	PointArrayObject[6]=-1.0f;
	PointArrayObject[7]=-1.0f;
	PointArrayObject[8]=-1.0f;
		
	// Vertex 2
	// Color
	PointArrayObject[9] =0.9f;
	PointArrayObject[10]=0.9f;
	PointArrayObject[11]=0.1f;

	// Normal
	PointArrayObject[12]= 1.0f;
	PointArrayObject[13]=-1.0f;
	PointArrayObject[14]=-1.0f;

	// Position
	PointArrayObject[15]= 1.0f;
	PointArrayObject[16]=-1.0f;
	PointArrayObject[17]=-1.0f;
	
	// Vertex 3
	// Color
	PointArrayObject[18]=0.9f;
	PointArrayObject[19]=0.1f;
	PointArrayObject[20]=0.1f;

	// Normal
	PointArrayObject[21]=-1.0f;
	PointArrayObject[22]= 1.0f;
	PointArrayObject[23]=-1.0f;

	// Position
	PointArrayObject[24]=-1.0f;
	PointArrayObject[25]= 1.0f;
	PointArrayObject[26]=-1.0f;
	
	// Vertex 4
	// Color
	PointArrayObject[27]=0.1f;
	PointArrayObject[28]=0.1f;
	PointArrayObject[29]=0.9f;

	// Normal
	PointArrayObject[30]= 1.0f;
	PointArrayObject[31]= 1.0f;
	PointArrayObject[32]=-1.0f;

	// Position
	PointArrayObject[33]= 1.0f;
	PointArrayObject[34]= 1.0f;
	PointArrayObject[35]=-1.0f;
	
	// Vertex 5
	// Color
	PointArrayObject[36]=0.9f;
	PointArrayObject[37]=0.1f;
	PointArrayObject[38]=0.1f;

	// Normal
	PointArrayObject[39]=-1.0f;
	PointArrayObject[40]=-1.0f;
	PointArrayObject[41]= 1.0f;

	// Position
	PointArrayObject[42]=-1.0f;
	PointArrayObject[43]=-1.0f;
	PointArrayObject[44]= 1.0f;
		
	// Vertex 6
	// Color
	PointArrayObject[45]=0.9f;
	PointArrayObject[46]=0.1f;
	PointArrayObject[47]=0.1f;

	// Normal
	PointArrayObject[48]= 1.0f;
	PointArrayObject[49]=-1.0f;
	PointArrayObject[50]= 1.0f;

	// Position
	PointArrayObject[51]= 1.0f;
	PointArrayObject[52]=-1.0f;
	PointArrayObject[53]= 1.0f;
	
	// Vertex 7
	// Color
	PointArrayObject[54]=0.1f;
	PointArrayObject[55]=0.9f;
	PointArrayObject[56]=0.1f;

	// Normal
	PointArrayObject[57]=-1.0f;
	PointArrayObject[58]= 1.0f;
	PointArrayObject[59]= 1.0f;

	// Position
	PointArrayObject[60]=-1.0f;
	PointArrayObject[61]= 1.0f;
	PointArrayObject[62]= 1.0f;
	
	// Vertex 8
	// Color
	PointArrayObject[63]=0.1f;
	PointArrayObject[64]=0.1f;
	PointArrayObject[65]=0.9f;

	// Normal
	PointArrayObject[66]= 1.0f;
	PointArrayObject[67]= 1.0f;
	PointArrayObject[68]= 1.0f;

	// Position
	PointArrayObject[69]= 1.0f;
	PointArrayObject[70]= 1.0f;
	PointArrayObject[71]= 1.0f;

	nt=6*2*3;
	int *IndiceArrayObject=new int[nt];
	// Face 1
	// Triangle 1
	IndiceArrayObject[0]=0;
	IndiceArrayObject[1]=1;
	IndiceArrayObject[2]=2;
	// Triangle 2
	IndiceArrayObject[3]=1;
	IndiceArrayObject[4]=2;
	IndiceArrayObject[5]=3;
	
	// Face 2
	// Triangle 1
	IndiceArrayObject[6]=4;
	IndiceArrayObject[7]=5;
	IndiceArrayObject[8]=0;
	// Triangle 2
	IndiceArrayObject[9 ]=5;
	IndiceArrayObject[10]=0;
	IndiceArrayObject[11]=1;
	
	// Face 3
	// Triangle 1
	IndiceArrayObject[12]=2;
	IndiceArrayObject[13]=3;
	IndiceArrayObject[14]=6;
	// Triangle 2
	IndiceArrayObject[15]=3;
	IndiceArrayObject[16]=6;
	IndiceArrayObject[17]=7;

	// Face 4
	// Triangle 1
	IndiceArrayObject[18]=0;
	IndiceArrayObject[19]=4;
	IndiceArrayObject[20]=2;
	// Triangle 2
	IndiceArrayObject[21]=2;
	IndiceArrayObject[22]=4;
	IndiceArrayObject[23]=6;

	// Face 5
	// Triangle 1
	IndiceArrayObject[24]=3;
	IndiceArrayObject[25]=1;
	IndiceArrayObject[26]=5;
	// Triangle 2
	IndiceArrayObject[27]=3;
	IndiceArrayObject[28]=5;
	IndiceArrayObject[29]=7;

	// Face 6
	// Triangle 1
	IndiceArrayObject[30]=4;
	IndiceArrayObject[31]=5;
	IndiceArrayObject[32]=6;
	// Triangle 2
	IndiceArrayObject[33]=6;
	IndiceArrayObject[34]=5;
	IndiceArrayObject[35]=7;

	// Generate And Bind The Vertex Buffer
	glGenBuffersARB( 1, &VBO_vertex );							// Get A Valid Name
	glBindBufferARB( GL_ARRAY_BUFFER_ARB, VBO_vertex );			// Bind The Buffer
	// Load The Data
	glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(float)*3*3*nv, PointArrayObject, GL_STATIC_DRAW_ARB );

	// Generate And Bind The Texture Coordinate Buffer
	glGenBuffersARB( 1, &VBO_triangle );							    // Get A Valid Name
	glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, VBO_triangle );	// Bind The Buffer
	// Load The Data
	glBufferDataARB( GL_ELEMENT_ARRAY_BUFFER_ARB, sizeof(GLuint)*nt, IndiceArrayObject, GL_STATIC_DRAW_ARB );

	delete PointArrayObject;
	delete IndiceArrayObject;

}
void Cylinder::RenderVBO()
{
  // Enable Pointers-      
  glEnable(GL_COLOR_MATERIAL);
	
  glEnableClientState( GL_VERTEX_ARRAY );						// Enable Vertex Arrays
  glEnableClientState( GL_COLOR_ARRAY );						// Enable Normal Arrays	
  glEnableClientState( GL_NORMAL_ARRAY );						// Enable Normal Arrays

  glBindBufferARB( GL_ARRAY_BUFFER_ARB, VBO_vertex);
  glVertexPointer( 3, GL_FLOAT, 9 * sizeof(float), ((float*)NULL + (6)) );
  glNormalPointer(    GL_FLOAT, 9 * sizeof(float), ((float*)NULL + (3)) );
  glColorPointer ( 3, GL_FLOAT, 9 * sizeof(float),  0);

  // Render
  glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, VBO_triangle);
  glDrawElements(GL_TRIANGLES, nt, GL_UNSIGNED_INT, 0);
  
  // Disable Pointers
  glDisableClientState( GL_NORMAL_ARRAY );						// Disable Normal Arrays
  glDisableClientState( GL_COLOR_ARRAY );						// Disable Normal Arrays
  glDisableClientState( GL_VERTEX_ARRAY );						// Disable Vertex Arrays
}


