// Affichage OpenGL

#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>										
#include <GL/glu.h>

#include <iostream>
#include <stdio.h>

#include "vector.h"

#include "shape.h"

// EDIT
#include "tree.h"
#define DALPHA          0.5
#define TREEMOVINGDELTA 0.05

Rectangle* ground = NULL;
std::vector< Rectangle* > environment;
std::vector< Tree* > trees;

// Identifiant de la racine de la scene
Node *scene=NULL;

// Angle de rotation des objects
float alpha=0.0f;

int Mousekey=0,Mousex,Mousey;

int light=1;
int rotate=0;

// Identifiant de fenetre
int window;

// Gestion du clavier.
void Keyboard(unsigned char key, int x, int y)
{
  // Si ESC
  if (key==27)
  {
    glutDestroyWindow(window); 
    exit(0);
  }
  // Gestion des lumieres
  if ((key=='l') || (key=='L'))
  {
    light = 1-light;
    if (!light) 
    {
      glDisable(GL_LIGHTING);
    } 
    else 
    {
      glEnable(GL_LIGHTING);
    }
  }

  // Rotation
  if ((key=='r') || (key=='R'))
  {
    rotate = 1-rotate;
  }

  glutPostRedisplay();
}

// Special keys: F*, arrow keys
void SpecialKeyboard(int key, int x, int y)
{
  // Use arrows to move tree
  if( key==GLUT_KEY_LEFT )
  {
    *trees[0] += Vector(-TREEMOVINGDELTA,0,0);
  }
  if( key==GLUT_KEY_RIGHT )
  {
    *trees[0] += Vector(TREEMOVINGDELTA,0,0);
  }
  if( key==GLUT_KEY_UP )
  {
    *trees[0] += Vector(0,0,-TREEMOVINGDELTA);
  }
  if( key==GLUT_KEY_DOWN )
  {
    *trees[0] += Vector(0,0,TREEMOVINGDELTA);
  }

  // Update trees after being moved
  for(unsigned i=0 ; i<trees.size() ; i++)
  {
    trees[i]->adaptToEnvironment( ground, environment );
  }

  glutPostRedisplay();
}

void Resize(int width, int height)
{
  // Evite de divisser par 0 en cas de changement de taille
  if (height==0)				
  {
    height=1;
  }
  // Fenetrage
  glViewport (0, 0, width, height);

  // Definition de la matrice de vue
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();  

  // Camera
  gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

  // Rebascule dans le repere du modele
  glMatrixMode(GL_MODELVIEW);
}

// Permet de definir une matiere a partir d'une couleur
void GlutShade(GLfloat r,GLfloat v,GLfloat b)
{
  // Couleur sans lumieres
  glColor3f(0.8,0.9,0.6);

  // Couleur avec lumieres
  GLfloat color[4];

  // La couleur diffuse sera egale a 25% de la couleur
  color[0]=0.75f*r;
  color[1]=0.75f*v;
  color[2]=0.75f*b;
  color[3]=1.0;

  glMaterialfv(GL_FRONT, GL_DIFFUSE, color);

  // La couleur ambiante sera egale a 25% de la couleur
  color[0]=0.25f*r;
  color[1]=0.25f*v;
  color[2]=0.25f*b;
  color[3]=1.0;

  glMaterialfv(GL_FRONT, GL_AMBIENT, color); // GL_AMBIENT_AND_DIFFUSE

  color[0]=1.0f;
  color[1]=0.0f;
  color[2]=0.0f;
  color[3]=1.0;

  glMaterialfv(GL_BACK, GL_AMBIENT_AND_DIFFUSE, color);
}

// Affichage
void GlutRendering()
{
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity (); // go to the origin

  glTranslatef(0.0f,-3.0f,-20.0f);	// (0.0f,0.0f,-10.0f)

  glRotatef(alpha,0.0f,1.0f,0.0f); // (angle, axis) // (alpha,1.0f,1.0f,1.0f)

  bool useVBO=false;
  if (useVBO)
  {
	scene->RenderVBO();
  }
  else
  {
	scene->Render();
  }

  // EDIT
  if( trees.size() != 0 )
  {
    trees[0]->generateJunctionSpheres();
  }

  glutSwapBuffers();
}

void MouseMove(int x, int y)
{
  Mousex = x;
  Mousey = y;
}

void MousePush(int button, int state, int x, int y)
{
}

// Rafraichissement
void GlutIdle(void)
{
  if (rotate)
  {
    alpha+=DALPHA;
  }
  GlutRendering();
}

//!Initialise OpenGL
void InitGlut(int width,int height)
{  
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_ALPHA | GLUT_DEPTH);

  // Fenetre initiale
  glutInitWindowSize(512, 512);  
  glutInitWindowPosition(0, 0);  
  window=glutCreateWindow("Real Time Rendering");

  glutDisplayFunc(GlutRendering);
  //glutFullScreen();

  // Rafraichissement
  glutIdleFunc(&GlutIdle);

  // Changement de taille
  glutReshapeFunc(Resize);

  // Clavier
  glutKeyboardFunc(Keyboard);
  glutSpecialFunc(SpecialKeyboard); // for special keyx as F* or arrows

  // Souris
  glutMouseFunc(MousePush);
  glutMotionFunc(MouseMove);

  // Initialise les parametres de rendu
  glClearColor (0.3f, 0.4f, 0.6f, 1.0);

  glClearDepth(1.0);
  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);	
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glShadeModel(GL_SMOOTH);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();				// Reset The Projection Matrix

  gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

  // Placement des lumieres dans l'espace du modele
  glMatrixMode(GL_MODELVIEW);

  GLfloat light_ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
  GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  GLfloat light_position[] = { 0.0,0.0, 10.0,0.0 };

  glLightfv (GL_LIGHT1, GL_AMBIENT, light_ambient);
  glLightfv (GL_LIGHT1, GL_DIFFUSE, light_diffuse);
  glLightfv (GL_LIGHT1, GL_POSITION, light_position);
  glEnable(GL_LIGHT0); 

  glEnable(GL_LIGHTING);

  // Elimination des facettes arriere
  //glCullFace(GL_BACK);
  //glEnable(GL_CULL_FACE);
}

int main(int argc,char **argv)
{
  glutInit(&argc, argv);
  InitGlut(512,512);
  glewInit();

//  scene=new Cube(Vector(-1,-1,-2),Vector(2,3,1));
//  scene=new Cylinder(Vector(0,0,0),Vector(0,0,1),0.5,30);

  // !! All volumes along y because of orientation of camera (along z at the beginning) hard to change

  // Ground
  ground = new Rectangle(Vector(-10,-0.1,-10),Vector(10,0,10));
  scene = ground;

  // Wall
  environment.push_back( new Rectangle(Vector(4,0,-4),Vector(5,8,6)) );
  scene = new Union( environment[0], scene );

  // Tree
  trees.push_back( new Tree(Vector(0,0,0), Vector(0,4,0), 0.15, 30, 4, 3) ); // a, b, r, n, nbSons, nbSteps // 0 < r < 1
  scene = new Union( trees[0], scene );

  glutMainLoop();

  delete scene; // with Union, all objects created will be deleted recursively
  
  return 0;
}